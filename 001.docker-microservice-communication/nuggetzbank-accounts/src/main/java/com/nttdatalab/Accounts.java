package com.nttdatalab;

import javax.annotation.PostConstruct;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

@Path("accounts")
public class Accounts {

    private Client client;
    private WebTarget target;

    @PostConstruct
    public void init() {
        this.client = ClientBuilder.newClient();
        this.target = this.client.target("http://nuggetzbanktransactions:8080/nuggetzbank-transactions/resources/transactions");
    }

    @GET
    public String account() {
        return "an account response plus: " + this.target.request(MediaType.TEXT_PLAIN).get(String.class);
    }
}
