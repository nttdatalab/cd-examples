# Docker Demo Readme

This demo creates two maven based java EE7 microservices and runs them in docker 
containers.  

The account microservice will:

- expose a public port which is accessible in the browser
- will internally communicate with the transaction microservice and return a merged piece of text from both services.  

The demo creates a user defined network in docker to enable the communcation.  The container name "nuggetzbanktransactions" 
given is significant as this is hard coded in the account microservice to discover the 
transaction microservice.


Steps to run the demo.  

Open a terminal.

Run following command from the directory: **[DOCKERFILES_REPO]/java**

	docker build -t nttdatalab/java .
	
Run following command from the directory: **[DOCKERFILES_REPO]/wildfly**

	docker build -t nttdatalab/wildfly .
	
Run following command from the directory: **[DOCKERFILES_REPO]/wildfly-configured**
	
	docker build -t nttdatalab/wildfly-configured .

Run following command from the directory: **[CD_EXAMPLES_REPO]/001.docker-microservice-communication/nuggetzbank-accounts**

	mvn install && docker build -t nttdatalab/nuggetzbank-accounts .

Run following command from the directory: **[CD_EXAMPLES_REPO]/001.docker-microservice-communication/nuggetzbank-transactions**

	mvn install && docker build -t nttdatalab/nuggetzbank-transactions .
	
Run following command

	docker network create nttdatalabnet
	
Run following command

	docker run -d --name nuggetzbanktransactions --net nttdatalabnet nttdatalab/nuggetzbank-transactions
	
Run following command

	docker run -d -p 8080:8080 --name nuggetzbankaccounts --net nttdatalabnet nttdatalab/nuggetzbank-accounts

Run following command

	curl -i http://192.168.99.100:8080/nuggetzbank-accounts/resources/accounts
	
You should see a response similar to:

	HTTP/1.1 200 OK
	Connection: keep-alive
	X-Powered-By: Undertow/1
	Server: WildFly/10
	Content-Type: application/octet-stream
	Content-Length: 48
	Date: Fri, 15 Sep 2017 07:30:51 GMT
	an account response plus: a transaction response

	